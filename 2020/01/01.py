def init():
    with open("input.txt", "r") as f:
        a = []
        for entry in f:
            a.append(int(entry))
    return a


def task_1():
    for i in challenge_input:
        for j in challenge_input:
            if i + j == 2020:
                return i * j


def task_1_more_efficient():
    for i in range(0, len(challenge_input)):
        for j in range(i, len(challenge_input)):
            if challenge_input[i] + challenge_input[j] == 2020:
                return challenge_input[i] * challenge_input[j]


def task_2():
    for i in challenge_input:
        for j in challenge_input:
            for k in challenge_input:
                if i + j + k == 2020:
                    return i * j * k


def task_2_more_efficient():
    for i in range(0, len(challenge_input)):
        for j in range(i, len(challenge_input)):
            for k in range(j, len(challenge_input)):
                if challenge_input[i] + challenge_input[j] + challenge_input[k] == 2020:
                    return challenge_input[i] * challenge_input[j] * challenge_input[k]


challenge_input = init()
print(f"Solution for 1.1: {task_1_more_efficient()}")
print(f"Solution for 1.2: {task_2_more_efficient()}")