def init():
    with open("input.txt", "r") as f:
        return f.read()


class SantaObject:
    x = 0
    y = 0

    def move(self, entry):
        if entry == "^":
            self.y += 1
        elif entry == ">":
            self.x += 1
        elif entry == "v":
            self.y -= 1
        elif entry == "<":
            self.x -= 1
        return self.x, self.y


def task_1():
    positions = [(0,0)]
    santa = SantaObject()
    for entry in challenge_input:
        pos = santa.move(entry)
        if pos not in positions:
            positions.append(pos)
    return len(positions)


def task_2():
    positions = [(0, 0)]
    santa = SantaObject()
    robot_santa = SantaObject()
    for i in range(0, len(challenge_input)):
        if i % 2 == 0:
            pos = santa.move(challenge_input[i])
        else:
            pos = robot_santa.move(challenge_input[i])
        if pos not in positions:
            positions.append(pos)
    return len(positions)


challenge_input = init()
print(f"Solution for 3.1: {task_1()}")
print(f"Solution for 3.2: {task_2()}")