def init():
    with open("input.txt", "r") as f:
        a = []
        for entry in f:
            a.append(entry)
    return a


def vowel_check(x):
    vowel_count = 0
    for entry in x:
        if entry in ["a", "e", "i", "o", "u"]:
            vowel_count += 1
    return vowel_count >= 3


def double_check(x):
    doubles = ['aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh', 'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr', 'ss', 'tt', 'uu', 'vv', 'ww', 'xx', 'yy', 'zz']
    for entry in doubles:
        if entry in x:
            return True
    return False


def forbidden_check(x):
    forbidden = ["ab", "cd", "pq", "xy"]
    for entry in forbidden:
        if entry in x:
            return False
    return True


def task_1():
    nice_count = 0
    for entry in challenge_input:
        if vowel_check(entry) and double_check(entry) and forbidden_check(entry):
            nice_count += 1
    return nice_count


def count_occurance(to_test, str_in):
    count = 0
    i = 0
    while i < len(str_in)-1:
        if str_in[i:i+2] == to_test:
            count += 1
            i += 1
        i += 1
    return count


def occurance_check(str_in):
    i = 0
    while i < len(str_in)-1:
        if count_occurance(str_in[i:i+2], str_in) >= 2:
            return True
        i += 1
    return False


def spaced_check(str_in):
    i = 0
    while i < len(str_in)-2:
        if str_in[i] == str_in[i + 2]:
            return True
        i += 1
    return False


def task_2():
    count = 0
    for entry in challenge_input:
        if occurance_check(entry) and spaced_check(entry):
            count += 1
    return count


challenge_input = init()
print(f"Solution for 5.1: {task_1()}")
print(f"Solution for 5.2: {task_2()}")