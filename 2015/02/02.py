def init():
    with open("input.txt", "r") as f:
        a = []
        for entry in f:
            a.append(entry)
    return a


def surface(x):
    # l w h
    a = x[0] * x[1]
    b = x[1] * x[2]
    c = x[2] * x[0]
    result = 2 * a + 2 * b + 2 * c + min([a,b,c])
    return result


def task_1():
    result = 0
    for entry in challenge_input:
        temp = [int(x) for x in entry.split("x")]
        result += surface(temp)
    return result


def task_2():
    result = 0
    for entry in challenge_input:
        temp = [int(x) for x in entry.split("x")]
        temp.sort()
        result += 2 * temp[0] + 2 * temp[1] + temp[0] * temp[1] * temp[2]
    return result


challenge_input = init()
print(f"Solution for 2.1: {task_1()}")
print(f"Solution for 2.2: {task_2()}")