import hashlib


def init():
    with open("input.txt", "r") as f:
        a = f.read()
    return a


def task_1():
    last_input = -1
    last_result = "xxxxxxx"
    while not last_result.startswith("00000"):
        last_input += 1
        int_str = challenge_input+str(last_input)
        last_result = hashlib.md5(int_str.encode("ascii")).digest().hex()
    return str(last_input)


def task_2():
    last_input = -1
    last_result = "xxxxxxx"
    while not last_result.startswith("000000"):
        last_input += 1
        int_str = challenge_input + str(last_input)
        last_result = hashlib.md5(int_str.encode("ascii")).digest().hex()
    return str(last_input)


challenge_input = init()
print(f"Solution for 4.1: {task_1()}")
print(f"Solution for 4.2: {task_2()}")