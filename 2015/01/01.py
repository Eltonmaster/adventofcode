def init():
    with open("input.txt", "r") as f:
        a = f.read()
    return a


def task_1():
    floor = 0
    for entry in challenge_input:
        if entry == "(": floor += 1
        if entry == ")": floor -= 1
    return floor


def task_2():
    floor = 0
    count = 1
    for entry in challenge_input:
        if entry == "(": floor += 1
        if entry == ")": floor -= 1
        if floor < 0:
            return count
        count += 1



challenge_input = init()
print(f"Solution for 1.1: {task_1()}")
print(f"Solution for 1.2: {task_2()}")