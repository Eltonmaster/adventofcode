def init():
    with open("input.txt", "r") as f:
        a = []
        for entry in f:
            a.append(int(entry))
    return a


def task_1():
    count = 0
    for i in range(1, len(challenge_input)):
        if challenge_input[i] > challenge_input[i-1]:
            count += 1
    return count


def task_2():
    count = 0
    for i in range(0, len(challenge_input)-3):
        current_value = challenge_input[i] + challenge_input[i+1] + challenge_input[i+2]
        next_value = challenge_input[i+1] + challenge_input[i+2] + challenge_input[i+3]
        if next_value > current_value:
            count += 1
    return count


challenge_input = init()
print(f"Solution for 1.1: {task_1()}")
print(f"Solution for 1.2: {task_2()}")
