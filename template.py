def init():
    with open("input.txt", "r") as f:
        a = []
        for entry in f:
            a.append(int(entry))
    return a


def task_1():
    pass


def task_2():
    pass


challenge_input = init()
print(f"Solution for 1.1: {task_1()}")
print(f"Solution for 1.2: {task_2()}")